import requests, uuid, json, time

TODOIST_API_TOKEN = "<your-todoist-api-token>"
GITLAB_API_TOKEN = "<your-gitlab-api-token>"
GITLAB_PROJECT_ID = "<your-gitlab-project-id>"
SECTION_ID = 0000000

# Get GitLab Issues list
url = "https://gitlab.com/api/v4/projects/" + GITLAB_PROJECT_ID + "/issues?state=opened"
headers = {"PRIVATE-TOKEN": GITLAB_API_TOKEN}
r = requests.get(url, headers=headers)
while r.status_code != 200:
    r = requests.get(url, headers=headers)
    time.sleep(500)
myjson = json.loads(r.content)
issue_list = [{"id": str(i["id"]), "title": i["title"]} for i in myjson]

# Get all existing task in GitLab Section
todoist_task_json = requests.get(
    "https://api.todoist.com/rest/v1/tasks",
    params={"section_id": SECTION_ID},
    headers={"Authorization": "Bearer %s" % TODOIST_API_TOKEN},
).json()

# Dict w/ GitLab ID as Key, and dict Todo_ID, Todo_Title as value
task_gitlab_id_list = {
    i["content"].split("_")[0]: {
        "id": str(i["id"]),
        "title": i["content"].split("_")[1],
    }
    for i in todoist_task_json
}

# Compare Gitlab & Todoist issues and add/update
for gitlab_issue in issue_list:
    if gitlab_issue["id"] in task_gitlab_id_list.keys():
        # Update Title
        if gitlab_issue["title"] != task_gitlab_id_list[gitlab_issue["id"]]["title"]:
            r = requests.post(
                "https://api.todoist.com/rest/v1/tasks/"
                + task_gitlab_id_list[gitlab_issue["id"]]["id"],
                data=json.dumps(
                    {"content": gitlab_issue["id"] + "_" + gitlab_issue["title"]}
                ),
                headers={
                    "Content-Type": "application/json",
                    "X-Request-Id": str(uuid.uuid4()),
                    "Authorization": "Bearer %s" % TODOIST_API_TOKEN,
                },
            )

    # Add Issue
    else:
        r = requests.post(
            "https://api.todoist.com/rest/v1/tasks",
            data=json.dumps(
                {
                    "content": gitlab_issue[0] + "_" + gitlab_issue[1],
                    "section_id": SECTION_ID,
                }
            ),
            headers={
                "Content-Type": "application/json",
                "X-Request-Id": str(uuid.uuid4()),
                "Authorization": "Bearer %s" % TODOIST_API_TOKEN,
            },
        )
